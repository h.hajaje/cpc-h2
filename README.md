# CPC-H2: Convolution Power based Cryptosystem.
Convolution Power Cryptosystem ([CPC-H2](https://gitlab.com/h.hajaje/cpc-h2)) is based on a novel hard problem
that is proven to be resistant to attacks by quantum algorithms. 

[Copyright 2021](https://gitlab.com/h.hajaje/cpc-h2).

