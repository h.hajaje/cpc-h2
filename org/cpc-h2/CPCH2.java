package org.cpch2;

import java.math.BigInteger;
import java.util.ArrayList;

/**
 * Convolution Power Cryptosystem (CPC-H2) is based on a novel hard problem that
 * is proven to be resistant to attacks by quantum algorithms. 
 * @article CPC-H2: Convolution Power based Cryptosystem and Digital Signature. 
 * @Copyright 2021.
 * 
 */
public class CPCH2 {
    //Parameters of CPC-H2.
    static int k ; 
    static int N ;
    static int p ;
    static int q ;
    
    static int alpha = 1;
    static int n ;
    static int pn ;
    //Matrix of powers such that P[row][column]=row^column % n.
    int[][] P;
    //Matrix of products such that M[row][column]=row*column % n.
    int[][] M;
    //The polynomial "a" such that the products of all elements of "a" is pseudo
    //generator.
    int[] a;
    //The polynomial "b" is the private key.
    int[] b;
    // f is the bijection from {x in Z_n/ x%p!=0 and x%q!=0} to {0,1,...,pn-1}.
    public static ArrayList<Integer> f;
    // If is the inverse of f.
    int[] If;

    public CPCH2(int p, int q, int k, int N) throws Exception {
        System.out.println("Parametres CPC-H2:");
        System.out.println("p="+p);
        System.out.println("q="+q);
        System.out.println("n=p*q="+(p*q));
        System.out.println("k="+k);
        System.out.println("N="+N);
        this.p = p;
        this.q = q;
        this.N = N;
        this.k = k;
        if (!BigInteger.valueOf(p).isProbablePrime(999999999)) {
            throw new Exception("the number " + p + " isn't a prime number! please put a prime number for p");
        }
        if (!BigInteger.valueOf(q).isProbablePrime(999999999)) {
            throw new Exception("the number " + q + " isn't a prime number! please put a prime number for q");
        }

        n = p * q;
        pn = (p - 1) * (q - 1);
        a = new int[N];
        alpha = gcd(p - 1, q - 1);
        if (N <= k) {
            throw new Exception("the number N should be greater strictly than k.");
        }

        if (k <= pn / alpha) {
            throw new Exception("the number k should be greater strictly than " + pn / alpha + ".");
        }
        f = new ArrayList();
        If = new int[n];
        P = new int[pn][pn / alpha];
        M = new int[pn][pn];
    }
    
    // function for calculate gcd of two numbers a and b.
    int gcd(int a, int b) {
        if (a == 0) {
            return b;
        }
        return gcd(b % a, a);
    }
    
    // function for test if g is a pseudo generator in Z_n
    boolean isPseudoGenerator(int g) {
        int i = 1;
        while (i < pn / alpha) {
            if (P[If[g]][i] == 1) {
                return false;
            }
            i++;
        }
        return true;
    }
    
    //function for calculate convolution power.
    int[] ConvolutionPower(int[] a, int[] b) {
        int[] r = new int[N];
        for (int i = 0; i < N; i++) {
            r[i] = 1;
            for (int j = 0; j < N; j++) {
                r[i] = M[If[r[i]]][If[P[If[a[j]]][b[(-j + i + N) % N]]]];
            }
        }
        return r;
    }
    //function for calculate two convolution powers a^b and c^b.
    int[][] ConvolutionPowerTwo(int[] a,int[] c, int[] b) {
        int[][] r = new int[2][N];
        for (int i = 0; i < N; i++) {
            r[0][i] = r[1][i] = 1;
            for (int j = 0; j < N; j++) {
                int B=b[(-j + i + N) % N];
                r[0][i] = M[If[r[0][i]]][If[P[If[a[j]]][B]]];
                r[1][i] = M[If[r[1][i]]][If[P[If[c[j]]][B]]];
            }
        }
        return r;
    }
    
    //function the extended euclid used for calculate the inverse of a number "b" in "Z_a".
    public static Integer[] ExtendedEuclid(Integer a, Integer b) {
        Integer[] ans = new Integer[3];
        Integer q;

        if (b == 0) {
            ans[0] = a;
            ans[1] = 1;
            ans[2] = 0;
        } else {
            q = a / b;
            ans = ExtendedEuclid(b, a % b);
            Integer temp = ans[1] - ans[2] * q;
            ans[1] = ans[2];
            ans[2] = temp;
        }

        return ans;
    }
    
    //function used for decrypted message.
    int[] multiplyC(int[] a, int[] b, int[] abr) {
        int[] r = new int[N];
        for (int i = 0; i < N; i++) {
            Integer[] t = ExtendedEuclid(n, b[i]);
            r[i] = If[M[If[a[i]]][If[((t[0] * t[2]) % n + n) % n]]] - If[abr[i]];
        }
        return r;
    }
    
    //function used for crypted message.
    int[] multiplyM(int[] a, int[] m, int[] abr) {
        int[] r = new int[N];
        for (int i = 0; i < N; i++) {
            r[i] = M[If[a[i]]][(m[i] + If[abr[i]]) % pn];
        }
        return r;
    }

    public void initParameters() {

        ArrayList<Integer> help = new ArrayList();
        for (int i = 0; i < n; i++) {
            if (gcd(i, n) == 1) {
                help.add(i);
            }
        }
        while (!help.isEmpty()) {
            Integer h = help.remove(((int) (Math.random() * 99999999)) % help.size());
            f.add(h);
            If[h] = f.size() - 1;
            for (int j = 0; j < P[If[h]].length; j++) {
                P[If[h]][j] = BigInteger.valueOf(h).modPow(BigInteger.valueOf(j), BigInteger.valueOf(n)).intValue();
            }
        }
        for (int i = 0; i < n; i++) {
            if (gcd(i, n) == 1) {
                for (int j = 0; j < n; j++) {
                    if (gcd(j, n) == 1) {
                        M[If[i]][If[j]] = (i * j) % n;
                    }
                }
            }
        }
        ArrayList<Integer> ls2 = new ArrayList();
        ArrayList<Integer> ls1 = (ArrayList<Integer>) f.clone();
        ls2 = new ArrayList();
        int l = 0;
        int s = 1;
        while (l < k) {
            Integer u = ls1.remove(((int) (Math.random() * 9999999)) % ls1.size());
            s = (s * u) % n;
            ls2.add(u);
            l++;
        }
        while (l < N - 1) {
            Integer u = f.get(((int) (Math.random() * 9999999)) % f.size());
            ls2.add(u);
            s = (s * u) % n;
            l++;
        }
        Integer u = f.get(((int) (Math.random() * 9999999)) % f.size());
        while (!isPseudoGenerator(BigInteger.valueOf(s).multiply(BigInteger.valueOf(u)).mod(BigInteger.valueOf(n)).intValue())) {
            u = f.get(((int) (Math.random() * 9999999)) % f.size());
        }
        ls2.add(u);
        for (int i = 0; i < N; i++) {
            a[i] = ls2.remove(((int) (Math.random() * 9999999)) % ls2.size());
        }
    }

    public int[] generateKeys() {
        b = new int[N];
        for (int i = 0; i < N; i++) {
            b[i] = ((int) (Math.random() * 99999)) % (pn / alpha);
        }
        return ConvolutionPower(a, b);
    }

    public int[][] crypter(int[] h, int[] m) {
        int[] r = new int[N];
        for (int i = 0; i < N; i++) {
            r[i] = ((int) (Math.random() * 99999)) % (pn / alpha);
        }
        int[][] a2 = ConvolutionPowerTwo(a,h, r);
        return new int[][]{multiplyM(a2[1], m, a2[1]), a2[0]};
    }

    public int[] decrypter(int[][] c) {
        int[] abr = ConvolutionPower(c[1], b);
        return multiplyC(c[0], abr, abr);
    }

    public static void print(int[] o) {
        for (int i = 0; i < o.length; i++) {
                System.out.print(o[i] + "\t");
            }
            System.out.println();
    }

    

}
