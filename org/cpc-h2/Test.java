package org.cpch2;

import java.util.Date;

/**
 *
 * @author hajaje hamid
 */
public class Test {

    public static void main(String[] args) {
        try {
            //CPCH2 cpch2 = new CPCH2(11, 31, 126, 167);
            //CPCH2 cpch2 = new CPCH2(19, 37, 207, 269);
            //CPCH2 cpch2 = new CPCH2(31, 61, 432, 523);
            CPCH2 cpch2 = new CPCH2(37, 73, 851, 1019);
            
            Date d1 = new Date();
            cpch2.initParameters();
            Date d2 = new Date();
            System.out.println("Init Parameters Finished in " + (d2.getTime() - d1.getTime()) + " ms");
            Date d3 = new Date();
            int[] h = cpch2.generateKeys();
            Date d4 = new Date();
            System.out.println("Generate Keys Finished in " + (d4.getTime() - d3.getTime()) + " ms");
            int[] m = new int[cpch2.N];
            for (int i = 0; i < cpch2.N; i++) {
                m[i] = ((int) (Math.random() * 99999)) % cpch2.pn;
            }
            Date d5 = new Date();
            int[][] c = cpch2.crypter(h, m);
            Date d6 = new Date();
            System.out.println("Crypt Message in " + (d6.getTime() - d5.getTime()) + " ms");

            Date d7 = new Date();
            int[] d = cpch2.decrypter(c);
            Date d8 = new Date();
            System.out.println("Decrypt Message in " + (d8.getTime() - d7.getTime()) + " ms");
            System.out.println("Message Original:");
            cpch2.print(m);
            System.out.println("Message Crypted:");
            cpch2.print(c[0]);
            cpch2.print(c[1]);
            System.out.println("Message Decrypted:");
            cpch2.print(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
